<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;

class AreaMicro extends BaseMicroService
{
    # 查询 楼栋所属小区
    public function getNeighborhoodByBuilding($code)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getNeighborhoodByBuilding',
            [
                'code' => $code,
            ]
        );
    }

    # 查询 街道下的小区
    public function getNeighborhoods($code, $skip = 0, $limit = 100)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getNeighborhoods',
            [
                'code' => $code,
                'skip' => $skip,
                'limit' => $limit,
            ]
        );
    }

    // 应用查询
    public function search($pid = -1, $code = '', $keyword = '', $skip = 0, $limit = 100, $isFullName = 1)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/search',
            [
                'keyword' => $keyword,
                'pid' => $pid,
                'skip' => $skip,
                'limit' => $limit,
                'code' => $code,
                'isFullName' => $isFullName
            ]
        );
    }

    // 获取房屋列表
    public function searchRooms($neighborhood_id = 0, $community_code = '', $building_code = '', $unit_code = '',
                                $skip = 0, $limit = 100)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/searchRooms',
            [
                'neighborhood_id' => $neighborhood_id,
                'community_code' => $community_code,
                'skip' => $skip,
                'limit' => $limit,
                'building_code' => $building_code,
                'unit_code' => $unit_code,
            ]
        );
    }

    // 应用查询
    public function getByName($appName = '')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getByName',
            [
                'name' => $appName
            ]
        );
    }

    // 应用查询
    public function getCommunitys($codes = '')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getCommunitys',
            [
                'codes' => $codes
            ]
        );
    }

    // 应用查询
    public function getLikeName($appName = '')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getLikeName',
            [
                'name' => $appName
            ]
        );
    }

    # 根据坐标查询所在社区-第三方版
    public function getCommunityByGps($longitude, $latitude)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getCommunityByGps',
            [
                'lon' => $longitude,
                'lat' => $latitude
            ]
        );
    }

    # 根据坐标查询所在社区-本地版
    public function getCommunityByLocalGps($longitude, $latitude)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getCommunityByLocalGps',
            [
                'lon' => $longitude,
                'lat' => $latitude
            ]
        );
    }


    // 应用查询
    public function getByKeyword($keyword = '')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/search',
            [
                'keyword' => $keyword
            ]
        );
    }

    // 应用查询
    public function getByCode($code = '')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getByCode',
            [
                'code' => $code,
            ]
        );
    }


    //房屋详情
    public function getRoom($id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/getRoom',
            [
                'id' => $id,
            ]
        );
    }
}
