<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;
use Illuminate\Support\Arr;

class OrgMicro extends BaseMicroService
{
    /*
    |--------------------------------------------------------------------------
    | 员工
    |--------------------------------------------------------------------------
    */

    /**
     * 新增员工
     *
     * @param array $data [realname, hire_type=>雇佣类型，可选值：'外包','临时','合同','在编', hiredate=>入职日期]
     */
    public function employeeHireCy(array $data)
    {
        $this->isSet($data, ['realname', 'hire_type', 'hiredate']);

        $data = Arr::add($data, 'userid', '');
        $data = Arr::add($data, 'mobile', '');
        $data = Arr::add($data, 'internal_email', '');
        $data = Arr::add($data, 'gender', '');
        $data = Arr::add($data, 'education', '');
        $data = Arr::add($data, 'school', '');
        $data = Arr::add($data, 'married', '');
        $data = Arr::add($data, 'political', '');
        $data = Arr::add($data, 'birthday', '');
        $data = Arr::add($data, 'idcard', '');
        $data = Arr::add($data, 'memo', '');
        $data = Arr::add($data, 'ondate', '');

        return $this->post('employee/hire', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | 组织
    |--------------------------------------------------------------------------
    */

    /**
     * 组织列表 - 获取指定框架的组织节点列表
     *
     * @param array $data [family_id=> 框架id] | ['appid', 'skip', 'limit', 'family_id']
     * @return mixed
     */
    public function familyorgSearch(array $data = [])
    {
        $this->isSet($data, ['family_id']);

        if (!isset($data['appid'])) {
            if (!empty(env('APP_ID', ''))) $data['appid'] = env('APP_ID');
        }

        $data = Arr::add($data, 'parentid', '');
        $data = Arr::add($data, 'skip', '');
        $data = Arr::add($data, 'limit', '');
        $data = Arr::add($data, 'family_id', '');

        return $this->post('familyorg/search', $data);
    }

    /**
     * 组织节点查询 - 获取指定框架的组织节点详情
     *
     * @param array $data | ['name', 'status', 'org_type', 'skip', 'limit', 'longitude1', 'longitude2', 'latitude1', 'latitude2']
     * @return mixed
     */
    public function cyOrgSearch(array $data = [])
    {
        $data = Arr::add($data, 'name', '');
        $data = Arr::add($data, 'status', -1);
        $data = Arr::add($data, 'org_type', -1);
        $data = Arr::add($data, 'skip', 0);
        $data = Arr::add($data, 'limit', 100);
        $data = Arr::add($data, 'longitude1', 0);
        $data = Arr::add($data, 'longitude2', 0);
        $data = Arr::add($data, 'latitude1', 0);
        $data = Arr::add($data, 'latitude2', 0);

        return $this->post('org/search', $data);
    }

    /**
     * 获取指定父节点和名称的节点
     *
     * @param array $data [parent_id=> 父节点ID, name=> 节点名称, family_id=> 组织框架ID]
     * @return mixed
     */
    public function familyorgGetByName(array $data)
    {
        $this->isSet($data, ['parent_id', 'name', 'family_id']);

        return $this->post('familyorg/getByName', $data);
    }

    /**
     * 新增节点
     *
     * @param array $data [name=> 节点名称, org_type=> 组织节点类型] | ['latitude', 'longitude', 'head', 'area_id', 'area_name']
     */
    public function orgAdd(array $data)
    {
        $this->isSet($data, ['name', 'org_type']);

        $data = Arr::add($data, 'latitude', '');
        $data = Arr::add($data, 'longitude', '');
        $data = Arr::add($data, 'head', '');
        $data = Arr::add($data, 'area_id', '');
        $data = Arr::add($data, 'area_name', '');

        return $this->post('org/add', $data);
    }

    /**
     * 修改节点
     *
     * @param array $data [id=> UUID] | ['name', 'status', 'latitude', 'longitude', 'head', 'area_id', 'area_name']
     */
    public function orgModify(array $data)
    {
        $this->isSet($data, ['id']);

        $data = Arr::add($data, 'name', '');
        $data = Arr::add($data, 'status', '');
        $data = Arr::add($data, 'latitude', '');
        $data = Arr::add($data, 'longitude', '');
        $data = Arr::add($data, 'head', '');
        $data = Arr::add($data, 'area_id', '');
        $data = Arr::add($data, 'area_name', '');

        return $this->post('org/modify', $data);
    }

    /**
     * 删除节点
     */
    public function orgRemove(string $id)
    {
        $data['id'] = $id;

        return $this->post('org/remove', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | 组织框架
    |--------------------------------------------------------------------------
    */

    /**
     * 组织框架列表
     *
     * @return mixed
     */
    public function familySearch(array $data = [])
    {
        $data = Arr::add($data, 'parentid', 0);
        $data = Arr::add($data, 'recursion', 0);

        return $this->post('/family/search', $data);
    }

    /**
     * 新增组织框架
     *
     * @param array $data [name=> 中文名, ename=> 英文名, parentid=>父级组织节点id]
     */
    public function familyAdd(array $data)
    {
        $this->isSet($data, ['name', 'ename', 'parentid']);

        return $this->post('family/add', $data);
    }

    /**
     * 将节点绑定到组织下
     *
     * @param array $data [org_uuid=>节点id, org_type=>组织类型, typeid=>组织框架id, name=>节点名称]
     */
    public function familyorgBind(array $data)
    {
        $this->isSet($data, ['org_uuid', 'typeid', 'name', 'org_type', 'parentid']);

        return $this->post('familyorg/bind', $data);
    }

    /**
     * 组织类型列表
     *
     * @param array $data [无必填] |
     * ['skip', 'limit']
     * @return mixed
     */
    public function orgtypeSearch($data = [])
    {
        $data = Arr::add($data, 'skip', '');
        $data = Arr::add($data, 'limit', '');

        return $this->post('orgtype/search', $data);
    }

    /**
     * 节点列表，可以递归查询
     *
     * @param array $data [family_id=>框架id] | [parent_id, recursion]
     */
    public function familyorgGetList(array $data)
    {
        $this->isSet($data, ['family_id']);

        $data = Arr::add($data, 'parent_id', '');
        $data = Arr::add($data, 'recursion', 1);

        return $this->post('familyorg/getList', $data);
    }

    //组织节点列表

    /**
     * @param $appId
     * @param $name
     * @param int $skip
     * @param int $limit
     * @return mixed
     * @property array $data['a', 'b']
     */
    public function search($appId, $name, $skip = 0, $limit = 20)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/org/search',
            [
                'appid' => $appId,
                'name' => $name,
                'status' => -1,
                'org_type' => -1,
                'longitude1' => 0,
                'latitude1' => 0,
                'longitude2' => 0,
                'latitude2' => 0,
                'skip' => $skip,
                'limit' => $limit
            ]
        );
    }

    //指定框架下组织节点列表
    public function nodeList($appId, $familyId, $parentId, $skip = 0, $limit = 20)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/familyorg/search',
            [
                'appid' => $appId,
                'family_id' => $familyId,
                'parentid' => $parentId,
                'skip' => $skip,
                'limit' => $limit
            ]
        );
    }

    //组织节点详情
    public function nodeDetail($familyId, $uuid)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/familyorg/get',
            ['uuid' => '1',
                'family_id' => $familyId,
                'uuid' => $uuid
            ]
        );
    }

    //创建组织节点
    public function nodeCreate($name, $orgType)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/org/add',
            [
                'name' => $name,
                'latitude' => '',
                'longitude' => '',
                'head' => '',
                'area_id' => '',
                'area_name' => '',
                'org_type' => $orgType
            ]
        );
    }

    //修改组织节点
    public function nodeUpdate($uuid, $name)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/org/modify',
            [
                'id' => $uuid,
                'name' => $name,
                // 'latitude' => '',
                // 'longitude' => '',
                // 'head' => '',
                // 'area_id' => '',
                // 'area_name' => '',
                // 'org_type' => '',
                // 'status' => ''
            ]
        );
    }

    /*
    //删除组织节点
    public function nodeDelete($mobile, $content, $sign, $type = 0)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/org/remove',
            [
                'id' => '',
                'sign' => $sign,
                'type' => $type,
            ]
        );
    }
    */

    //绑定组织节点和框架 typeid=51公益架构
    public function nodeBindFrame($orgName, $orgUuid, $orgType)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/familyorg/bind',
            [
                'org_uuid' => $orgUuid,
                'typeid' => 52,
                'name' => $orgName,
                'org_type' => $orgType,
                'status' => 1
            ]
        );
    }

    /*
    //解绑组织节点和框架
    public function nodeUnbindFrame($mobile, $content, $sign, $type = 0)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/familyorg/unbind',
            [
                'uuid' => '1',
                'family_id' => '1',
                'sign' => $sign,
                'type' => $type,
            ]
        );
    }
    */


    //员工列表
    public function employeeList($familyId, $realName, $userUuid, $orgId, $levelId, $positionUuid, $skip = 0, $limit = 200)
    {
        $data = [];
        if ($familyId) {
            $data['familyid'] = $familyId;
        }
        if ($realName) {
            $data['realname'] = $realName;
        }
        if ($orgId) {
            $data['orgid'] = $orgId;
        }
        if ($userUuid) {
            $data['user_uuid'] = $userUuid;
        }
        if ($levelId) {
            $data['levelid'] = $levelId;
        }
        if ($positionUuid) {
            $data['position_uuid'] = $positionUuid;
        }
        if ($familyId) {
            $data['familyid'] = $familyId;
        }
        $data['skip'] = $skip;
        $data['limit'] = $limit;
        return AGRequest::getInstance()->post(
            $this->host,
            '/employee/search',
            $data
        );
    }

    //员工列表
    public function employeeGet($id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/employee/get',
            [
                'id' => $id
            ]
        );
    }


    //员工列表
    public function employeeHire($userId, $realName, $mobile, $internalEmail, $gender, $education,
                                 $school, $married, $political, $birthday, $idCard, $hireType, $memo, $hiredDate, $onDate)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/employee/hire',
            [
                'userid' => $userId,
                'realname' => $realName,
                'mobile' => $mobile,
                'internal_email' => $internalEmail,
                'gender' => $gender,
                'education' => $education,
                'school' => $school,
                'married' => $married,
                'political' => $political,
                'birthday' => $birthday,
                'idcard' => $idCard,
                'hire_type' => $hireType,
                'memo' => $memo,
                'hiredate' => $hiredDate,
                'ondate' => $onDate,
                'limit' => $gender,
                'limit' => $gender
            ]
        );
    }

    //员工列表
    public function employeeFire($id, $fireDate)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/employee/fire',
            [
                'id' => $id,
                'firedate' => $fireDate
            ]
        );
    }
}
