<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\BaseMicroService;
use RdKafka\Conf;
use RdKafka\KafkaConsumer;
use RdKafka\Producer;
use RdKafka\TopicConf;

class KafkaMicro extends BaseMicroService
{
    public function produce($topic, $playload, $key, $headers=null, $user=false, $password=false): bool
    {
        if (!extension_loaded('rdkafka')) {
            throw new \Exception("no rdkafka extension", 1);
        }

        $conf = new Conf();

        $conf->set('sasl.mechanisms', 'PLAIN');
        $conf->set('api.version.request', 'true');
        $conf->set('sasl.username', $user);
        $conf->set('sasl.password', $password);
        $conf->set('security.protocol', 'SASL_PLAINTEXT');
        $conf->set('metadata.broker.list', $this->env('KAFKA_BROKER_LIST'));
        $conf->set('message.send.max.retries', 5);
        $rk = new Producer($conf);
        $rk->setLogLevel(LOG_INFO);
        $rk->addBrokers(env('KAFKA_BROKER_LIST'));
        $topic = $rk->newTopic($topic);
        $topic->producev(RD_KAFKA_PARTITION_UA, 0, $playload, $key, $headers, time());
        $rk->poll(0);
        $result = $rk->flush(2000);
        if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $topics
     * @param $group_id
     * @param $callback
     * @param $user
     * @param $password
     * @param $duration
     * @return void
     * @throws \RdKafka\Exception
     */
    public function consume(Array $topics, $group_id, $callback, $user=false, $password=false, $duration=-1){
        if (!extension_loaded('rdkafka')) {
            throw new \Exception("no rdkafka extension", 1);
        }

        $conf = new Conf();
        $conf->set('session.timeout.ms', 10000);
        $conf->set('request.timeout.ms', 30000);
        $conf->set('socket.timeout.ms', 30001);
        $conf->set('group.id', $group_id);

        $conf->set('sasl.mechanisms', 'PLAIN');
        $conf->set('api.version.request', 'true');
        $conf->set('sasl.username', $user);
        $conf->set('sasl.password', $password);
        $conf->set('security.protocol', 'SASL_PLAINTEXT');
        $conf->set('metadata.broker.list', env('KAFKA_BROKER_LIST'));

        $topicConf = new TopicConf();
        $conf->setDefaultTopicConf($topicConf);

        $consumer = new KafkaConsumer($conf);
        $consumer->subscribe($topics);

        $end_time = 9999999999;
        if($duration != -1){
            $end_time = time() + $duration;
        }
        while(time() < $end_time){
            $message = $consumer->consume(5 * 1000);
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    $callback($message);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    break;
                default:
                    throw new \Exception($message->errstr(), $message->err);
            }
        }
    }
}
