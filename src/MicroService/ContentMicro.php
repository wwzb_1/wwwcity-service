<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;
use Illuminate\Support\Arr;

class ContentMicro extends BaseMicroService
{
    /**
     * 文本内容审核
     * @param array $data
     * ['tasks'=> '要审核的内容，以数组或对象的形式传入'，
     * 'bizType'=> '检查场景
     *              default：默认
     *              news：新闻
     *              comment：评论'
     * ]
     * @param $tasks
     * @param string $bizType
     * @return mixed
     */
    public function scan(array $data)
    {
        $this->isSet($data, 'tasks');

        $data = Arr::add($data, 'bizType', 'news');
        $data['tasks'] = json_encode($data['tasks']);

        return AGRequest::getInstance()->post(
            $this->host,
            '/security/text/scan',
            $data
        );
    }
}
