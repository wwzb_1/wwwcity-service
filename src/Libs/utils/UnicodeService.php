<?php
namespace Cy\WWWCityService\Libs\utils;

class UnicodeService
{
    /**
     * 需要转义的字符
     * @var string[]
     */
    private $alphabet_encode_map = [
        '0'=>'Q','1'=>'E','2'=>'Y','3'=>'T','4'=>'O','5'=>'P',
        '6'=>'A','7'=>'S','8'=>'H','9'=>'K','A'=>'V','B'=>'M',
        'C'=>'N','D'=>'C','E'=>'X','F'=>'Z',
    ];
    /**
     * 转义后的字符，对应转义字符列,数量与上面字符相同且不重复
     * @var string[]
     */
    private $alphabet_decode_map;

    protected static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function __construct()
    {
        $this->alphabet_decode_map = array_flip($this->alphabet_encode_map);
    }

    /**
     * 字符串编码
     * @param string $str
     * @param bool $encrypt
     * @return string|string[]
     */
    public function encode($str, $encrypt=true){
        $str = strtoupper(bin2hex($str));
        if(!$encrypt){
            return $str;
        }
        $result = '';
        for ($i=0;$i<strlen($str);$i++){
            $result.=$this->alphabet_encode_map[$str[$i]];
        }
        return $result;
    }

    /**
     * 字符串解密
     * @param $str
     * @param bool $decrypt
     * @return false|string
     */
    public function decode($str, $decrypt=true){
        $result = '';
        if($decrypt){
            for ($i=0;$i<strlen($str);$i++){
                $result .= $this->alphabet_decode_map[$str[$i]];
            }
        }else{
            $result = $str;
        }
        return hex2bin($result);
    }
}
