<?php

namespace Cy\WWWCityService\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Cy\WWWCityService\WWWCityService service(string $config_name)
 */
class WWWCityService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'WWWCityService';
    }
}
